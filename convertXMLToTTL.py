import requests
import json

# Définir le chemin vers le fichier XML
fichier_xml = "./PO2.xml"

files = {
    'data': open(fichier_xml, 'rb'),
}

# Requête POST vers l'API
url = "https://chowlk.linkeddata.es/api"

response = requests.post(url, files=files)

# Vérifier le code de statut
if response.status_code == 200:
    # Analyser la réponse JSON
    data = json.loads(response.content)

    # Extraire la clé ttl_data
    ttl_data = data["ttl_data"]
    error = data["errors"]
    warning = data["warnings"]

    print(f"errors : {error}")
    print(f"warnings : {warning}")
    # Enregistrer le contenu dans un fichier
    with open("PO2.ttl", "w") as f:
        f.write(ttl_data)
        print("**Succès de l'appel**")
else:
    print(f"**Échec** : Erreur lors de l'appel à l'API ({response.status_code})")